package rwmutex

import (
	"math"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRwMutex(t *testing.T) {
	count := int(math.Pow(2, 20.0))

	rwMutexDuration, mutexDuration := RwMutex(count)
	assert.True(t, rwMutexDuration < mutexDuration)
}
