package rwmutex

import (
	"sync"
	"time"
)

func RwMutex(count int) (time.Duration, time.Duration) {
	var m sync.RWMutex

	return test(count, &m, m.RLocker()), test(count, &m, &m)
}

func test(count int, mutex, rwMutex sync.Locker) time.Duration {
	var wg sync.WaitGroup
	wg.Add(count + 1)

	beginTestTime := time.Now()
	go slow(&wg, mutex)

	for i := count; i > 0; i-- {
		go fast(&wg, rwMutex)
	}

	wg.Wait()
	return time.Since(beginTestTime)
}

func slow(wg *sync.WaitGroup, l sync.Locker) {
	defer wg.Done()

	for i := 5; i > 0; i-- {
		l.Lock()
		l.Unlock()

		time.Sleep(1)
	}
}

func fast(wg *sync.WaitGroup, l sync.Locker) {
	defer wg.Done()

	l.Lock()
	defer l.Unlock()
}
