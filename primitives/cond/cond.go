package cond

import (
	"sync"
	"time"
)

func Cond() int {
	startTime := time.Now()
	result := 0

	c := sync.NewCond(&sync.Mutex{})
	c.L.Lock()

	go func() {
		time.Sleep(500 * time.Millisecond)

		c.Broadcast()

		time.Sleep(501 * time.Millisecond)

		c.Broadcast()
	}()

	for conditionTrue(startTime) == false {
		result++

		c.Wait()
	}

	c.L.Unlock()

	return result
}

func conditionTrue(startTime time.Time) bool {
	return time.Since(startTime) > time.Second
}

func Button() int {
	result := 0
	type Button struct {
		Clicked *sync.Cond
	}

	button := Button{Clicked: sync.NewCond(&sync.Mutex{})}

	var clickRegistered sync.WaitGroup
	clickRegistered.Add(3)

	subscribe(button.Clicked, func() {
		result += 5
		clickRegistered.Done()
	})

	subscribe(button.Clicked, func() {
		result += 7
		clickRegistered.Done()
	})

	subscribe(button.Clicked, func() {
		result += 11
		clickRegistered.Done()
	})

	button.Clicked.Broadcast()
	clickRegistered.Wait()

	return result
}

func subscribe(c *sync.Cond, fn func()) {
	var goroutineRunning sync.WaitGroup
	goroutineRunning.Add(1)

	go func() {
		goroutineRunning.Done()
		c.L.Lock()
		defer c.L.Unlock()

		c.Wait()
		fn()

		time.Sleep(time.Second)
	}()

	goroutineRunning.Wait()
}
