package cond

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCond(t *testing.T) {
	got := Cond()

	assert.Equal(t, 2, got)
}

func TestButton(t *testing.T) {
	got := Button()

	assert.Equal(t, 23, got)
}
