package once

import (
	"sync"
)

func Once() int {
	var result int
	increment := func() {
		inc(&result)
	}

	var once sync.Once
	var increments sync.WaitGroup

	increments.Add(100)
	for i := 0; i < 100; i++ {
		go func() {
			defer increments.Done()

			once.Do(increment)
		}()
	}

	increments.Wait()

	return result
}

func inc(val *int) {
	*val++
}

func OnceMore() {
	var onceA, onceB sync.Once
	var initB func()

	initA := func() { onceB.Do(initB) }
	initB = func() { onceA.Do(initA) }

	onceA.Do(initA)
}
