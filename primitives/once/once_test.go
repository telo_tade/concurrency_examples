package once

import "fmt"

func ExampleOnce() {
	fmt.Println(Once())
	// Output: 1
}

func ExampleOnceMore() {
	OnceMore()
	// Output:
}
