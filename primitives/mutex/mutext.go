package mutex

import (
	"sync"
)

func Mutex() int {
	var result int
	var lock sync.Mutex

	increment := func() {
		lock.Lock()
		defer lock.Unlock()

		result++
	}
	decrement := func() {
		lock.Lock()
		defer lock.Unlock()

		result--
	}

	// Increment
	var arithmetic sync.WaitGroup
	for i := 0; i <= 50; i++ {
		arithmetic.Add(1)
		go func() {
			defer arithmetic.Done()

			increment()
		}()
	}

	// Decrement
	for i := 0; i <= 5; i++ {
		arithmetic.Add(1)
		go func() {
			defer arithmetic.Done()

			decrement()
		}()
	}

	arithmetic.Wait()

	return result
}
