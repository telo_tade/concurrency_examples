package waitgroup

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestWaitgroup(t *testing.T) {
	got := WaitGroup()

	assert.Equal(t, 2, got)
}

func ExampleWaitgroup() {
	fmt.Println(WaitGroup())
	// Output: 2
}
