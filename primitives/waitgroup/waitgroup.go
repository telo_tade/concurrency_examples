package waitgroup

import (
	"sync"
	"time"
)

func WaitGroup() int {
	result := 0

	var wg sync.WaitGroup
	wg.Add(2)

	go func() {
		defer wg.Done()

		result++
		time.Sleep(1)
	}()

	go func() {
		defer wg.Done()

		result++
		time.Sleep(2)
	}()

	wg.Wait()

	return result
}
