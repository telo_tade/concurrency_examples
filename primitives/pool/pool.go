package pool

import (
	"sync"
)

func Pool() int {
	result := 0

	myPool := &sync.Pool{
		New: func() interface{} {
			result++
			return struct{}{}
		},
	}

	myPool.Get()
	instance := myPool.Get()

	myPool.Put(instance)
	myPool.Get()

	return result
}
