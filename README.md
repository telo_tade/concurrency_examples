# concurrency_examples

Golang concurrency examples, inspired from "Concurrency in Go" by Katherine Cox-Buday. Run each example individually.