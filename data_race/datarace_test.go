package data_race

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestExample(t *testing.T) {
	got := ConcurrentAccess()

	assert.Equal(t, 1, got)
}

func TestLoopCounterRace(t *testing.T) {
	got := LoopCounterRace()

	assert.Equal(t, 10, got)
}

func TestSharedVariable(t *testing.T) {
	got := SharedVariable()

	assert.Equal(t, 0, got)
}

func TestChannelClose(t *testing.T) {
	got := ChannelClose()

	assert.Equal(t, 11, got)
}
