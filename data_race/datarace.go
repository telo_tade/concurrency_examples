package data_race

import (
	"sync"
	"time"
)

func ConcurrentAccess() int {
	var data int

	go func() {
		data++
	}()

	time.Sleep(time.Second)

	return data
}

func LoopCounterRace() int {
	var wg sync.WaitGroup
	wg.Add(5)

	result := 0

	for i := 0; i < 5; i++ {
		go func() {
			result += i
			wg.Done()
		}()
	}

	wg.Wait()

	return result
}

func SharedVariable() int {
	var wg sync.WaitGroup
	wg.Add(2)

	result := 0

	go func() {
		result = 1

		wg.Done()
	}()

	go func() {
		result = 0

		wg.Done()
	}()

	wg.Wait()

	return result
}

func ChannelClose() int {
	c := make(chan int, 1)
	result := 0

	go func() {
		c <- 11
		result = 11
	}()

	close(c)

	return result
}
