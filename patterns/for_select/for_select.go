package for_select

import (
	"sync"
)

func Compute() string {
	done := make(chan int)
	data := make(chan string)

	go func() {
		buildData(done, data)
		close(data)
	}()

	result := ""
	var wg sync.WaitGroup
	wg.Add(1)

	go func(wg *sync.WaitGroup) {
		for input := range data {
			result += input
		}

		wg.Done()
	}(&wg)

	wg.Wait()

	return result
}

func buildData(done <-chan int, result chan<- string) {
	data := []string{"a", "b", "c"}

	for _, s := range data {
		select {
		case <-done:
			return
		default:
			result <- s
		}
	}

	return
}
