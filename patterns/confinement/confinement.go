package confinement

func producer() <-chan int {
	result := make(chan int, 4)

	go func() {
		defer close(result)

		for i := 0; i < 10; i++ {
			result <- i
		}
	}()

	return result
}

func consumer(input <-chan int) int {
	result := 0

	for val := range input {
		result += val
	}

	return result
}

func ProcessData() int {
	data := producer()

	return consumer(data)
}
