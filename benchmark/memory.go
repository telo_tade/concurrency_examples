package memory

import (
	"runtime"
	"sync"
)

func Memory() (uint64, uint64) {
	var c <-chan interface{}
	var wg sync.WaitGroup
	noop := func() { wg.Done(); <-c }

	wg.Add(1)
	before := memConsumed()

	go noop()

	wg.Wait()
	after := memConsumed()

	return before, after
}

func memConsumed() uint64 {
	runtime.GC()

	var s runtime.MemStats
	runtime.ReadMemStats(&s)

	return s.Sys
}
