package memory

import (
	"fmt"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMemory(t *testing.T) {
	before, after := Memory()

	fmt.Printf("before %v, after %v, difference %v\n", before/1000, after/1000, (after-before)/1000)

	assert.Less(t, before, after)
}

func BenchmarkContextSwitch(b *testing.B) {
	var wg sync.WaitGroup
	begin := make(chan struct{})
	c := make(chan struct{})
	var token struct{}

	sender := func() {
		defer wg.Done()
		<-begin

		for i := 0; i < b.N; i++ {
			c <- token
		}
	}

	receiver := func() {
		defer wg.Done()
		<-begin

		for i := 0; i < b.N; i++ {
			<-c
		}
	}

	wg.Add(2)

	go sender()
	go receiver()

	b.StartTimer()
	close(begin)

	wg.Wait()
}
