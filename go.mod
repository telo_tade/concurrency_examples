module gitlab.com/telo_tade/concurrency_examples

go 1.17

require (
	github.com/stretchr/testify v1.7.1
	golang.org/x/time v0.0.0-20220411224347-583f2d630306
)

require (
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
