package ratelimit

import (
	"context"
	"sort"
	"time"

	"golang.org/x/time/rate"
)

func Compute(limitOne int, limitTwo int) int {
	limiter := MultiLimiter(rate.NewLimiter(rate.Limit(1), limitOne), rate.NewLimiter(rate.Limit(1), limitTwo))

	result := 0
	ticker := time.Tick(time.Second)

infinite:
	for {
		select {
		case <-ticker:
			break infinite
		default:
			limiter.Wait(context.Background())
			result++
		}
	}

	return result
}

type RateLimiter interface {
	Wait(context.Context) error
	Limit() rate.Limit
}

func MultiLimiter(limiters ...RateLimiter) *multiLimiter {
	byLimit := func(i, j int) bool {
		return limiters[i].Limit() < limiters[j].Limit()
	}
	sort.Slice(limiters, byLimit)
	return &multiLimiter{limiters: limiters}
}

type multiLimiter struct {
	limiters []RateLimiter
}

func (l *multiLimiter) Wait(ctx context.Context) error {
	for _, l := range l.limiters {
		if err := l.Wait(ctx); err != nil {
			return err
		}
	}
	return nil
}
func (l *multiLimiter) Limit() rate.Limit {
	return l.limiters[0].Limit()
}
