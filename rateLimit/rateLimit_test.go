package ratelimit

import "fmt"

func ExampleLimit() {
	fmt.Println(Compute(4, 10))

	// Output: 5
}
