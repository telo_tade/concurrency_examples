package deadlock

import (
	"sync"
	"time"
)

type value struct {
	mu    sync.Mutex
	value int
}

func Deadlock() {
	var one, two value

	var wg sync.WaitGroup
	wg.Add(2)

	go deadlock(&one, &two, wg)
	go deadlock(&two, &one, wg)

	wg.Wait()
}

func deadlock(one, two *value, wg sync.WaitGroup) {
	one.mu.Lock()
	defer one.mu.Unlock()

	time.Sleep(time.Second)

	two.mu.Lock()
	defer two.mu.Unlock()

	wg.Done()
}
