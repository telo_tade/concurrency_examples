package livelock

import (
	"bytes"
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)

func Livelock() {
	var left, right int32
	tryLeft := func(out *bytes.Buffer) bool { return tryDir("left", &left, out) }
	tryRight := func(out *bytes.Buffer) bool { return tryDir("right", &right, out) }

	walk := func(walking *sync.WaitGroup, name string) {
		var out bytes.Buffer
		defer func() { fmt.Println(out.String()) }()
		defer walking.Done()

		fmt.Fprintf(&out, "%v is trying to scoot:", name)
		for i := 0; i < 5; i++ {
			if tryLeft(&out) || tryRight(&out) {
				return
			}
		}

		fmt.Fprintf(&out, "\n%v tosses her hands up in exasperation!", name)
	}

	var peopleInHallway sync.WaitGroup
	peopleInHallway.Add(2)

	go walk(&peopleInHallway, "Alice")
	go walk(&peopleInHallway, "Barbara")

	peopleInHallway.Wait()
}

func tryDir(dirName string, direction *int32, out *bytes.Buffer) bool {
	fmt.Fprintf(out, " %v", dirName)

	atomic.AddInt32(direction, 1)

	time.Sleep(time.Millisecond)

	if atomic.LoadInt32(direction) == 1 {
		fmt.Fprint(out, ". Success!")
		return true
	}

	time.Sleep(time.Millisecond)

	atomic.AddInt32(direction, -1)

	return false
}

func takeStep(cadence *sync.Cond) {
	cadence.L.Lock()
	cadence.Wait()
	cadence.L.Unlock()
}
