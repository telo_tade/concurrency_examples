package closure

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestClosure(t *testing.T) {
	got := Closure()

	assert.Equal(t, 1, got)
}
