package closure

import (
	"sync"
)

func Closure() int {
	var wg sync.WaitGroup
	result := 0

	wg.Add(1)
	go func() {
		defer wg.Done()

		result = 1
	}()

	wg.Wait()

	return result
}
