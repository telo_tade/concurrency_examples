package starvation

import (
	"sync"
	"time"
)

func Starvation() (int, int) {
	var wg sync.WaitGroup
	var sharedLock sync.Mutex

	wg.Add(2)

	var result1, result2 int
	go greedyWorker(&wg, &sharedLock, &result1)
	go politeWorker(&wg, &sharedLock, &result2)

	wg.Wait()

	return result1, result2
}

func greedyWorker(wg *sync.WaitGroup, sharedLock *sync.Mutex, result *int) {
	defer wg.Done()

	var count int
	for begin := time.Now(); time.Since(begin) <= time.Second; {
		sharedLock.Lock()
		time.Sleep(3 * time.Nanosecond)
		sharedLock.Unlock()

		count++
	}

	*result = count
}

func politeWorker(wg *sync.WaitGroup, sharedLock *sync.Mutex, result *int) {
	defer wg.Done()

	var count int
	for begin := time.Now(); time.Since(begin) <= time.Second; {
		sharedLock.Lock()
		time.Sleep(1 * time.Nanosecond)
		sharedLock.Unlock()

		sharedLock.Lock()
		time.Sleep(1 * time.Nanosecond)
		sharedLock.Unlock()

		sharedLock.Lock()
		time.Sleep(1 * time.Nanosecond)
		sharedLock.Unlock()

		count++
	}

	*result = count
}
