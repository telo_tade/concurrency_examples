package starvation

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStarvation(t *testing.T) {
	got1, got2 := Starvation()

	assert.Equal(t, got1, got2)
}
